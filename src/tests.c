#include <sys/mman.h>
#include <unistd.h>

#include "tests.h"
#include "util.h"
#include "mem.h"
#include "mem_internals.h"

static struct block_header* block_get_header(void* contents) {
    return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

void* heap = NULL;
void* heap1 = NULL;

void test1() {
    debug("test1:\n");
    heap = heap_init(10000);

    debug_heap(stdout, heap);
    if (heap == NULL) err("test 1 failed\n");
    else debug("test 1 passed\n");
}

void test2() {
    debug("test2:\n");
    _malloc(10);
    void* block2 = _malloc(50);
    _malloc(100);

    debug_heap(stdout, heap);
    _free(block2);

    if(!block_get_header(block2)->is_free) err("test 2 failed\n");
    debug_heap(stdout, heap);
    debug("test 2 passed\n");
}

void test3() {
    debug("test3:\n");
    _malloc(50);
    void* block4 = _malloc(200);
    void* block5 = _malloc(500);

    debug_heap(stdout, heap);
    _free(block4);
    _free(block5);

    if(!block_get_header(block4)->is_free || !block_get_header(block5)->is_free) err("test 3 failed\n");
    debug_heap(stdout, heap);
    debug("test 3 passed\n");
}

void test4() {
    debug("test4:\n");
    void* block1 = _malloc(12000);
    debug_heap(stdout, heap);
    void* block2 = _malloc(500);
    debug_heap(stdout, heap);
    struct block_header* header1 = block_get_header(block1);
    struct block_header* header2 = block_get_header(block2);
    if ((uint8_t *)header1->contents + header1->capacity.bytes != (uint8_t*) header2)
        err("test 4 failed, new region is not after previous\n");
    debug("test 4 passed\n");
}

void test5() {
    debug("test5:\n");
    void* block1 = _malloc(11500);
    debug_heap(stdout, heap);

    struct block_header* header1 = block_get_header(block1);
    void* new_addr = (uint8_t *)header1->contents + header1->capacity.bytes;
    void* map_addr = mmap((void *) round_pages((size_t) new_addr), 1000, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED, 0, 0 );
    if(map_addr == MAP_FAILED) err("test 5 can't be finished\n");

    void* block2 = _malloc(1000);
    struct block_header* header2 = block_get_header(block2);
    if (new_addr == (uint8_t*) header2)
        err("test 5 failed, new region is after previous\n");

    debug_heap(stdout, heap);
    debug("test 5 passed\n");
}

